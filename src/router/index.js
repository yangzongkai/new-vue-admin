import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: "/login" },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/login.vue')
  },
  {
    path: '/home',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '../views/home.vue'),
    redirect: "/home/users",
    children: [
      { path: "users", name: "users", component: () => import(/* webpackChunkName: "users" */ '../views/user/users.vue'), },
      { path: "rights", name: "rights", component: () => import(/* webpackChunkName: "rights" */ '../views/rights/rights.vue'), },
      { path: "roles", name: "roles", component: () => import(/* webpackChunkName: "roles" */ '../views/rights/roles.vue'), },
      { path: "goods", name: "goods", component: () => import(/* webpackChunkName: "goods" */ '../views/goods/goods.vue'), },
      { path: "params", name: "params", component: () => import(/* webpackChunkName: "params" */ '../views/goods/params.vue'), },
      { path: "categories", name: "categories", component: () => import(/* webpackChunkName: "categories" */ '../views/goods/categories.vue'), },
      { path: "orders", name: "orders", component: () => import(/* webpackChunkName: "orderList" */ '../views/order/orders.vue'), },
      { path: "reports", name: "reports", component: () => import(/* webpackChunkName: "reports" */ '../views/reports/reports.vue'), },
      { path: "goods/add", name: "goods-add", component: () => import(/* webpackChunkName: "add" */ '../views/goods/goodsAdd.vue'),},
      { path: "goods/edit", name: "goods-edit", component: () => import(/* webpackChunkName: "edit" */ '../views/goods/goodsEdit.vue'),},
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
