import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function initState(){
  return {
    token:localStorage.token||"",
    username:localStorage.username||"",
  }
}

export default new Vuex.Store({
  state: {
    token:localStorage.token||"",
    username:localStorage.username||"",
  },
  mutations: {
    setToken(state,data){
      state.token=data
      localStorage.setItem("token",data)
    },
    setUsername(state,data){
      state.username=data
      localStorage.setItem("username",data)
    },
    resetState(state,){
      Object.assign(state,initState())
    }
  },
  actions: {
  },
  modules: {
  }
})
