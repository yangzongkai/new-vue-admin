import axios from "axios"
import { Message } from "element-ui"
import store from "../store"
import router from "../router"
import { login } from "./api"
const baseURL = "http://localhost:8888/api/private/v1/"
const instance = axios.create({ baseURL })
// axios.defaults.baseURL="http://localhost:8888/api/private/v1/";

//请求拦截器
instance.interceptors.request.use((config) => {
    if (store.state.token) {
        config.headers["Authorization"] = store.state.token
    }
    // else{
    //     router.push({name:"login"})
    // }
    return config
}, (err) => {
    return Promise.reject(err)
})

//响应拦截器
// instance.interceptors.request.use((response)=>{
//     console.log(response)
//     if(response.data.meta.status==='无效token'){
//         router.push({name:"login"})
//     }
//     return response
// },(error)=>{
//     return Promise.reject(error)
// })

export function http(url, method, data, params) {
    return new Promise((resolve, reject) => {
        instance({
            url,
            method,
            data,
            params
        }).then(res => {
            if ((res.status >= 200 && res.status < 300) || res.status == 304) {
                if ((res.data.meta.status >= 200 && res.data.meta.status < 300) || res.data.meta.status == 304) {
                    resolve(res.data)
                } else {
                    reject(res.data.meta)
                    Message({
                        showClose: true,
                        message: res.data.meta.msg,
                        type: "error"
                    })
                }
            } else {
                reject(res)
                Message({
                    showClose: true,
                    message: res.statusText,
                    type: "error"
                })
            }
        }).catch((err) => {
            reject(err)
            Message({
                showClose: true,
                message: err.msg,
                type: "error"
            })
        })
    })
}

//图片上传
export const uploadURL = baseURL + "upload"
