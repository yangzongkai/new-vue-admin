import { http } from "./index"
import { Message } from 'element-ui'

//登录方法
export function login(data) {
    return http("login", "post", data)
}

//获取权限列表
export function getMenus() {
    return http("menus", "get")
}

//获取用户列表
export function getUsers(params) {
    return http("users", "get", {}, params)
}

//添加用户
export function addUsers(data) {
    return http("users", "post", data)
}

//编辑用户提交
export function editUsers(id, data) {
    // console.log(id,data)
    return http("users/" + id, "put", data)
}

//获取角色列表
export function getRoles() {
    return http("roles", "get")
}

//分配用户角色
export function assignRoles(id, rid) {
    // console.log(id,rid)
    return http("users/" + id + "/role", "put", { rid })
}

//删除角色权限
export function deleteRights(roleId, rightId) {
    return http("roles/" + roleId + "/rights/" + rightId, "delete")
}

//获取rid
export function getRidById(id) {
    return http(`users/${id}`)
}
//删除用户
export function deleteUsers(id) {
    return http("users/" + id, "delete")
}
//修改用户状态
export function changeStatus(id, type) {
    return http(`users/${id}/state/${type}`, "put")
}

//获取权限列表
export function getRightsList() {
    return http("rights/list", "get")
}

//添加角色
export function addRoles(data) {
    return http("roles", "post", data)
}

//删除角色
export function deleteRoles(id) {
    return http("roles/" + id, "delete")
}

//编辑角色
export function editRoles(id, data) {
    return http("roles/" + id, "put", data)
}

//获取权限列表tree
export function getRightsTree() {
    return http("rights/tree", "get")
}

//分配角色
export function setRights(roleId, rids) {
    return http("roles/" + roleId + "/rights", "post", { rids })
}

//获取订单列表
export function getOrdersList(params) {
    return http("orders", "get", {}, params)
}

//获取商品列表
export function getGoodsList(params) {
    return http("goods", "get", {}, params)
}


//商品编辑提交
export function editGoods(id, params) {
    return http("goods/" + id, "put", params).then(res => {
        Message({
            showClose: true,
            message: res.meta.msg,
            type: "success"
        });
        return res
    })
}

//商品删除
export function deleteGoods(id){
    return http("goods/"+id,"delete")
}

//获取商品分类列表 
export function getGoodsCatList() {
    return http("categories", "get", {}, {
        type: 3,
    })
}

//获取分类参数列表 
export function getCatParamsList(id, sel = "many") {
    //sel="many"  es6的函数默认值
    return http(`categories/${id}/attributes`, "get", {}, {
        sel
    })
}

//添加商品
export function addGoods(data) {
    return http("goods", "post", data).then(res => {
        Message({
            showClose: true,
            message: res.meta.msg,
            type: "success"
        });
        return res
    })
}

//添加newtag,,,删除
export function addGoodsParamsTag(id, attrId, data) {
    return http(`categories/${id}/attributes/${attrId}`, "put", data).then(res => {
        Message({
            showClose: true,
            message: res.meta.msg,
            type: "success"
        });
        return res
    })
}

//删除参数
export function deleteParams(id,attrId){
    return http(`categories/${id}/attributes/${attrId}`,"delete")
}

//添加动态或静态参数
export function addGoodsParams(id,data){
    return http (`categories/${id}/attributes`,"post",data).then(res => {
        Message({
            showClose: true,
            message: res.meta.msg,
            type: "success"
        });
        return res
    })
}

//商品参数编辑提交
export function editGoodsParams(id,attrId,data){
    return http(`categories/${id}/attributes/${attrId}`,"put",data).then(res => {
        Message({
            showClose: true,
            message: res.meta.msg,
            type: "success"
        });
        return res
    })
}

//获取商品分类列表
export function getCateLists(params){
    return http ("categories","get",{},params)
}

// 添加商品分类
export function addCategories(data){
    return http ("categories","post",data).then(res => {
        Message({
            showClose: true,
            message: res.meta.msg,
            type: "success"
        });
        return res
    })
}

//删除商品分类
export function deleteCategories(id){
    return http ("categories/"+id,"delete")
}

//编辑商品分类
export function editCatgories(data){
    return http ("categories/"+data.cat_id,"put",data).then(res => {
        Message({
            showClose: true,
            message: res.meta.msg,
            type: "success"
        });
        return res
    })
}

//获取数据报表
export function getReports(){
    return http(`reports/type/1`,"get")
}
